;(function(){
	'use strict';
	
	$('form').removeClass("invisible");
	
	// jQuery items
    var $submit_btn = $("#submit-btn"),
		$solve_btn = $("#solve-btn"),
		$userChoicesField = $('#userChoices'),
		$choiceLabels = $(".choice-label"),
		$serialSelections,
		$submitHint = $('#submit-hint'),
		$endText = $(".end"),
		$userScoreTxt = $('#userScoreTxt'),
		$totalScoreTxt = $('#totalScoreTxt');
		
	// JS vars
    var submitted = false,
		answers_arr = [
			[0,1,0,0],//1
			[0,1,0,0],
			[0,0,1,0,0],
			[0,1,0,0],
			[1,0,0,0],//5
			[0,0,1,1],
			[0,0,0,1],
			[0,1,0,0],
			[1,0,0,0],
			[0,0,0,1],//10
			[1,0,0,0],
			[0,1,0,0],
			[0,1,0,0],
			[1,0,0,0],
			[0,0,0,1],//15
			[0,0,0,1],
			[0,1,0,0],
			[0,0,0,1],
			[1,0,0,0],
			[1,1,0,1,0]//20
		],
		shuffleList_arr = [1,1,0,0,1,1,1,1,1,1,1,0,1,1,0,1,1,0,1,0], // Which Qs should get shuffled? All except 3, 4, 12, 15, 18, 20
		userChoices_arr = initNewUserArray(),
		userScores_arr = [],
		userScore = 0;

    // Init stuff
    $submit_btn.disabled = true;
	
	shuffleChoices();
	shuffleQuestions();
	
	$choiceLabels.addClass("cursor-pointer");
	
	$solve_btn.on("click", solveQuiz);
	
	$('input:checkbox, input:radio').change(handleSubmitEnabling);

    $('form').on('submit', function (e) {
        if ($submit_btn.disabled || submitted) {
            e.preventDefault();
            return;
        }
		e.preventDefault();
		
		// Return list of only the checked items
		/*
		var set = $(":checkbox, :radio").map(function() {
			if (this.checked) {
				return this.id;
			}
		}).get().join();
		console.log("set: " + set);
		*/
		
		checkUser();
		userScore = tallyScore();
		
		// Show end text
		$endText.removeClass("hidden");
		$userScoreTxt.text(userScore);
		$totalScoreTxt.text(answers_arr.length);
		
		$(".submit").hide(0);
		scrollToBottom();
		
		$serialSelections = $("form").serialize();
		$userChoicesField.html("<h4>Serialized selections:</h4>" + $serialSelections);
		$userChoicesField.append("<h4>User's choices, score & choice text:</h4>" + getChoiceText().join("<br>"));
		$userChoicesField.append("<h4>Score: " + userScore + "</h4>");
		$userChoicesField.show(0);
		disableAll();
		
		// Remove hover class
		$(".choice-container").removeClass("choice-container").addClass("choice-container-disabled");
		// Remove pointer cursor
		$choiceLabels.removeClass("cursor-pointer");
		
		// Log
		console.log("Selections: " + $serialSelections);
		console.log("Score: " + userScore);
    });
	
	function shuffleChoices() {
		// What I want: For each .choices-container class, shuffle all the .choice-container classes
		// Option #1: http://jsfiddle.net/C6LPY/2/
		// Option #2: a jQuery plugin: https://css-tricks.com/snippets/jquery/shuffle-dom-elements/
		$(".choices-container").each(function(index) {
			if (shuffleList_arr[index]) {
				$(this).children().shuffle();
			}
		});
	}
	
	function shuffleQuestions() {
		var questions = $(".question");
		questions.shuffle();
		
		// Redo numbers, which get shuffled
		$(".question-number").each(function(index) {
			$(this).text(index + 1 + ".");
		});
	}
	
	function initNewUserArray() {
		// Init a blank user_arr
		var arr = [];
		for (var i=0; i<answers_arr.length; i++) {
			var ans = answers_arr[i];
			var user = [];
			for (var j=0; j<ans.length; j++) {
				user.push(0);
			}
			arr.push(user);
		}
		return arr;
	}
	
	function updateUserArray() {
		// For each Q, get the items that are checked
		for (var i=0; i<answers_arr.length; i++) {
			$("input[name='q" + (i+1) + "']:checked").each( function () {
				//console.log($(this).val());
				userChoices_arr[i][$(this).val()] = 1;
			});
		}
	}
	
	function checkUser() {
		updateUserArray();
		
		for (var i=0; i<answers_arr.length; i++) {
			// Compare user_arr to answers_arr
			// +1 point if same
			var qIsCorrect = arraysAreIdentical(answers_arr[i], userChoices_arr[i]);
			if (qIsCorrect) {
				userScores_arr.push(1);
			} else {
				userScores_arr.push(0);
			}
		}
	}
	
	function handleSubmitEnabling() {
		var answered = countAnsweredQuestions();
		
		if (answered === answers_arr.length) {
			// Enable
			$submit_btn.disabled = false;
			$submit_btn.removeClass("disabled");
		} else {
			// Disable
			$submit_btn.disabled = true;
			$submit_btn.addClass("disabled");
		}
		
		updateSubmitHint(answered, answers_arr.length);
	}
	
	function updateSubmitHint(answered, total) {
		$submitHint.html("You have have answered <b>" + answered + " of " + total + "</b> questions.");
	}
	
	function countAnsweredQuestions() {
		var answered = 0;
		
		for (var i=1; i<=answers_arr.length; i++) {
			// Get each input name collection, 'q1', 'q2', etc.
			// Then check that serialize() returns a string of a length of 0 (no selections)
			var q = $('input[name="q' + i + '"]:checked').serialize();
			var qLength = q.length;
			if (qLength > 0) {
				answered++;
			}
				
		}
		return answered;
	}
	
	function tallyScore() {
		var s = 0;
		for (var i=0; i<userScores_arr.length; i++)  {
			if (userScores_arr[i] === 1) {
				s++;
			}
		}
		return s;
	}
	
	function disableAll() {
		$('input:checkbox').prop('disabled', true);
		$('input:radio').prop('disabled', true);
	}
	
	function getChoiceText() {
		var choicesText_arr = [];
		for (var i=0; i<answers_arr.length; i++) {
			var ans = answers_arr[i];
			var ansText_arr = [];
			for (var j=0; j<ans.length; j++) {
				if (ans[j] === 1) {
					var $choice = $('#q'+(i+1)+'c'+(j+1));
					var $label = $("label[for='" + $choice.prop('id') + "']").text();
					ansText_arr.push($label);
				}
			}
			choicesText_arr.push("<strong>Q" + (i+1) + ":</strong> " + userChoices_arr[i] + " (+" + userScores_arr[i] + "), " + ansText_arr);
		}
		
		return choicesText_arr;
	}
	
	function solveQuiz() {
		for (var i=0; i<answers_arr.length; i++) {
			var ans = answers_arr[i];
			for (var j=0; j<ans.length; j++) {
				if (ans[j] === 1) {
					var $choice = $('#q'+(i+1)+'c'+(j+1));
					$choice.prop('checked', true);
				}
			}
		}
		
		scrollToBottom();
		handleSubmitEnabling();
	}
	
	// Array tests
	function arraysAreIdentical(arr1, arr2){
		if (arr1.length !== arr2.length) {
			return false;
		}
		
		for (var i = 0, len = arr1.length; i < len; i++){
			if (arr1[i] !== arr2[i]){
				return false;
			}
		}
		return true; 
	}
	
	// Scroll to bottom
	function scrollToBottom() {
		//window.scrollTo(0,document.body.scrollHeight);
		$("html, body").animate({
			scrollTop: $(document).height()
		}, 2000);
	}
	
})();